/**
 * 将字符串中的全角字符转换为半角字符。
 * @param {String} str 
 * @returns 
 */
function DBC2SBC(str)
{
    var result = "";
    for(var i = 0; i < str.length; i++)
    {
        code = str.charCodeAt(i);    //获取当前字符的unicode编码
        if (code >= 65281 && code <= 65373)   //在这个unicode编码范围中的是所有的英文字母已经各种字符
        {
            var d = str.charCodeAt(i) - 65248;
            result += String.fromCharCode(d);   //把全角字符的unicode编码转换为对应半角字符的unicode码
        }
        else if (code == 12288)//空格
        {
            var d = str.charCodeAt(i) - 12288 + 32;
            result += String.fromCharCode(d);
        }
        else
        {
            result += str.charAt(i);
        }
    }
    return result;
}

/**
 * 去除首尾空格符
 * @param {String} str 
 * @returns 
 */
function trimStr(str){
    return str.replace(/(^\s*)|(\s*$)/g,"");
}

var replaceSourceDataAddressX = 0
var replaceSourceDataAddressY = 0
var replaceSourceDataColumnsCount = 0
var replaceSourceDataRowsCount = 0
var replaceSourceSheetName = 0
var srcRange

var findAndReplaceRulesAddressX = 0
var findAndReplaceRulesAddressY = 0
var findAndReplaceRulesRowsCount = 0  // 只支持一列查找
var findAndReplaceRulesSheetName = 0
var findAndReplaceRulesRange

var replaceDataSourceWorkbook;
var findAndReplaceRulesWorkbook;

function onbuttonclick(idStr)
{
    if (typeof (wps.Enum) != "object") { // 如果没有内置枚举值
        wps.Enum = WPS_Enum
    }
    switch(idStr)
    {
        case "setReplaceRange":{
                //alert("setReplaceRange")
                replaceDataSourceWorkbook = wps.EtApplication().ActiveWorkbook;
                var aSheet = replaceDataSourceWorkbook.ActiveSheet;
                replaceSourceSheetName = aSheet.Name;
                var txtReplaceRangeSource = document.getElementById("txtReplaceRange");
                let s = wps.Application.Selection;
                if(s.Areas.Count != 1){
                    alert("复合替换功能只支持连续的区域！");
                    return;
                }
                var replaceSourceAddress = ("From: [" + replaceDataSourceWorkbook.Name + "] - " + replaceSourceSheetName + "\r\nRow: " + s.Row + ", Rows Count: " + s.Rows.Count + "\r\n" +
                                  "Column: " + s.Column + ", Columns Count: " + s.Columns.Count);
                //alert(srcAddress)
                txtReplaceRangeSource.value = replaceSourceAddress;
                replaceSourceRange = s;
                
                replaceSourceDataAddressY = s.Row;
                replaceSourceDataAddressX = s.Column;
                replaceSourceDataColumnsCount = s.Columns.Count;
                replaceSourceDataRowsCount = s.Rows.Count;

                break
            }
        case "setFindAndReplaceRulesRange":{              
                //alert("setFindAndReplaceRulesRange")
                findAndReplaceRulesWorkbook = wps.EtApplication().ActiveWorkbook;
                var aSheet = findAndReplaceRulesWorkbook.ActiveSheet;
                findAndReplaceRulesSheetName = aSheet.Name;

                var txtFindAndReplaceRulesRange = document.getElementById("txtFindAndReplaceRulesRange")
                let s = wps.Application.Selection
                if(s.Areas.Count != 1){
                    alert("用作【查找/替换】规则的区域必须是连续的！");
                    return;
                }

                if(s.Rows.Count < 1 || s.Columns.Count != 2){
                    alert("用作查找条件的数据区域不能少于 1 行，且必须是 2 列！");  // 其实根本不可能少于1行
                    return;
                }

                var findAndReplaceRulesAddress = ("Find&Replace With: [" + findAndReplaceRulesWorkbook.Name + "] - " + findAndReplaceRulesSheetName + "\r\nRow: " + s.Row + ", Rows Count: " + s.Rows.Count + "\r\n" +
                                  "Column: " + s.Column + ", Columns Count: " + s.Columns.Count );
                
                findAndReplaceRulesAddressX = s.Column;
                findAndReplaceRulesAddressY = s.Row;
                findAndReplaceRulesRowsCount = s.Rows.Count; 
                findAndReplaceRulesColumnsCount = s.Columns.Count; 
                findAndReplaceRulesRange = s;

                txtFindAndReplaceRulesRange.value = findAndReplaceRulesAddress;
                break
            }
        case "btnReplace":{
                if(replaceDataSourceWorkbook == undefined){
                    alert("未设置要在其中替换数据的区域！");
                    return;
                }
                if(findAndReplaceRulesWorkbook == undefined){
                    alert("未设置【查找/替换】规则所在的区域！");
                    return;
                }
                var replaceSourceSheets = replaceDataSourceWorkbook.Sheets;                
                var replaceSourceSheet;
                for(j=1; j<=replaceSourceSheets.Count; j++){   // 注意：这个索引是从1开始的！！！
                    var ss = replaceSourceSheets.Item(j);
                    if(ss==null)continue;
                    if(ss.Name == replaceSourceSheetName){
                        replaceSourceSheet = ss;
                        break;
                    }
                }                
                if(replaceSourceSheet == undefined){
                    alert('未设置要在其中替换数据的区域所在的数据表！');
                    return;
                }
                var findAndReplaceRulesRangeSheets = findAndReplaceRulesWorkbook.Sheets;
                var findAndReplaceRulesSheet;
                for(j=1; j<=findAndReplaceRulesRangeSheets.Count; j++){   // 注意：这个索引是从1开始的！！！
                    var fs = findAndReplaceRulesRangeSheets.Item(j);
                    if(fs==null)continue;
                    if(fs.Name == findAndReplaceRulesSheetName){
                        findAndReplaceRulesSheet = fs;
                        break;
                    }
                }
                if(findAndReplaceRulesSheet == undefined){
                    alert('未找到【查找/替换】规则区域所在数据表！');
                    return;
                }

                // 注意:坐标是先行后列!!!不是先X后Y!!!
                var replaceSourceRange = replaceSourceSheet.Range(replaceSourceSheet.Cells.Item(replaceSourceDataAddressY, replaceSourceDataAddressX), replaceSourceSheet.Cells.Item(replaceSourceDataAddressY + replaceSourceDataRowsCount - 1, replaceSourceDataAddressX + replaceSourceDataColumnsCount - 1));
                var replaceSourceArray = replaceSourceRange.Value2;
                var findAndReplaceRulesRange = findAndReplaceRulesSheet.Range(findAndReplaceRulesSheet.Cells.Item(findAndReplaceRulesAddressY, findAndReplaceRulesAddressX), findAndReplaceRulesSheet.Cells.Item(findAndReplaceRulesAddressY + findAndReplaceRulesRowsCount - 1, findAndReplaceRulesAddressX + findAndReplaceRulesColumnsCount - 1));
                var findAndReplaceRulesArray = findAndReplaceRulesRange.Value2;
                if(replaceSourceArray == null){
                    alert("设置的要在其中替换数据的区域中并没有可用来替换的数据!");
                    return;
                }
                if(findAndReplaceRulesArray == null){
                    alert("设置的【查找/替换】规则区域没有可用的数据！");
                    return;
                }

                for(let ri = 0; ri < replaceSourceArray.length; ri++){
                    var rowArray = replaceSourceArray[ri];
                    for(let ci = 0; ci < rowArray.length; ci++){
                        for(let fri = 0; fri < findAndReplaceRulesArray.length; fri++){
                            var ruleArray = findAndReplaceRulesArray[fri];
                            if(ruleArray.length != 2) continue;
                            rowArray[ci] = rowArray[ci].replace(ruleArray[0], ruleArray[1]);
                        }
                    }
                }

                replaceSourceRange.Value2 = replaceSourceArray;
                break;
            }
    }
}


window.onload = ()=>{
    // var xmlReq = WpsInvoke.CreateXHR();
    // var url = location.origin + "/.debugTemp/NotifyDemoUrl"
    // xmlReq.open("GET", url);
    // xmlReq.onload = function (res) {
    //     var node = document.getElementById("DemoSpan");
    //     window.document.getElementById("DemoSpan").innerHTML = res.target.responseText;
    // };
    // xmlReq.send();
}