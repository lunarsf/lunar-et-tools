//调用支付宝接口进行银行卡验证
$("#bank").blur(function(){
     var value = $(this).val(); //获取当前输入框的值
     $.post("https://ccdcapi.alipay.com/validateAndCacheCardInfo.json",{cardNo:value,cardBinCheck:'true'},function(res){		    
	 //console.log(res); //不清楚返回值的打印出来看
         //{"cardType":"DC","bank":"ICBC","key":"622200****412565805","messages":[],"validated":true,"stat":"ok"}
	 if(res.validated){
	   layer.msg('银行卡号正确',{icon:6});
	   return true; 
	 }else{
	   layer.msg('银行卡号错误',{icon:5});
	   setTimeout($("#bank").focus(),1); //获取焦点                   
	   return false;  
	 }
     },'json'); 
});

//银行及其简称，注意数组式json格式哈，取值方法略需修改
var bankList = {
    PSBC: "中国邮政储蓄银行", 
    ICBC: "中国工商银行", 
    ABC: "中国农业银行",  
    BOC: "中国银行", 
    CCB: "中国建设银行",    
    COMM: "中国交通银行",      
    CMB: "招商银行",
    CMBC: "中国民生银行",
    CEB: "中国光大银行",
    CITIC: "中信银行",
    HXBANK: "华夏银行",
    SPABANK: "深发/平安银行",
    CIB: "兴业银行",
    SHBANK: "上海银行",      
    SPDB: "浦东发展银行",
    GDB: "广发银行",
    BOHAIB: "渤海银行",
    GCB: "广州银行",
    JHBANK: "金华银行",
    WZCB: "温州银行",
    HSBANK: "徽商银行",
    JSBANK: "江苏银行",
    NJCB: "南京银行",
    NBBANK: "宁波银行",
    BJBANK: "北京银行",
    BJRCB: "北京农村商业银行",
    HSBC: "汇丰银行",
    SCB: "渣打银行",
    CITI: "花旗银行",
    HKBEA: "东亚银行",
    GHB: "广东华兴银行",
    SRCB: "深圳农村商业银行",
    GZRCU: "广州农村商业银行股份有限公司",
    DRCBCL: "东莞农村商业银行",
    BOD: "东莞市商业银行",
    GDRCC: "广东省农村信用社联合社",
    DSB: "大新银行",
    WHB: "永亨银行",
    DBS: "星展银行香港有限公司",
    EGBANK: "恒丰银行",
    TCCB: "天津市商业银行",
    CZBANK: "浙商银行",
    NCB: "南洋商业银行",
    XMBANK: "厦门银行",
    FJHXBC: "福建海峡银行",
    JLBANK: "吉林银行",
    HKB: "汉口银行",
    SJBANK: "盛京银行",
    DLB: "大连银行",
    BHB: "河北银行",
    URMQCCB: "乌鲁木齐市商业银行",
    SXCB: "绍兴银行",
    CDCB: "成都商业银行",
    FSCB: "抚顺银行",
    ZZBANK: "郑州银行",
    NXBANK: "宁夏银行",
    CQBANK: "重庆银行",
    HRBANK: "哈尔滨银行",
    LZYH: "兰州银行",
    QDCCB: "青岛银行",
    QHDCCB: "秦皇岛市商业银行",
    BOQH: "青海银行",
    TZCB: "台州银行",
    CSCB: "长沙银行",
    BOQZ: "泉州银行",
    BSB: "包商银行",
    DAQINGB: "龙江银行",
    SHRCB: "上海农商银行",
    ZJQL: "浙江泰隆商业银行",
    H3CB: "内蒙古银行",
    BGB: "广西北部湾银行",
    GLBANK: "桂林银行",
    DAQINGB: "龙江银行",
    CDRCB: "成都农村商业银行",
    FJNX: "福建省农村信用社联合社",
    TRCB: "天津农村商业银行",
    JSRCU: "江苏省农村信用社联合社",
    SLH: "湖南农村信用社联合社",
    JXNCX: "江西省农村信用社联合社",
    SCBBANK: "商丘市商业银行",
    HRXJB: "华融湘江银行",
    HSBK: "衡水市商业银行",
    CQNCSYCZ: "重庆南川石银村镇银行",
    HNRCC: "湖南省农村信用社联合社",
    XTB: "邢台银行",
    LPRDNCXYS: "临汾市尧都区农村信用合作联社",
    DYCCB: "东营银行",
    SRBANK: "上饶银行",
    DZBANK: "德州银行",
    CDB: "承德银行",
    YNRCC: "云南省农村信用社",
    LZCCB: "柳州银行",
    WHSYBANK: "威海市商业银行",
    HZBANK: "湖州银行",
    BANKWF: "潍坊银行",
    GZB: "赣州银行",
    RZGWYBANK: "日照银行",
    NCB: "南昌银行",
    GYCB: "贵阳银行",
    BOJZ: "锦州银行",
    QSBANK: "齐商银行",
    RBOZ: "珠海华润银行",
    HLDCCB: "葫芦岛市商业银行",
    HBC: "宜昌市商业银行",
    HZCB: "杭州商业银行",
    JSBANK: "苏州市商业银行",
    LYCB: "辽阳银行",
    LYB: "洛阳银行",
    JZCBANK: "焦作市商业银行",
    ZJCCB: "镇江市商业银行",
    FGXYBANK: "法国兴业银行",
    DYBANK: "大华银行",
    DIYEBANK: "企业银行",
    HQBANK: "华侨银行",
    HSB: "恒生银行",
    LSB: "临沂商业银行",
    YTCB: "烟台商业银行",
    QLB: "齐鲁银行",
    BCCC: "BC卡公司",
    CYB: "集友银行",
    TFB: "大丰银行",
    AEON: "AEON信贷财务亚洲有限公司",
    MABDA: "澳门BDA"
  }      